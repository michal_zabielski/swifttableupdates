//
//  FirstViewController.swift
//  Price update
//
//  Created by Michał Zabielski on 18.04.2016.
//  Copyright © 2016 X Open Hub Limited. All rights reserved.
//

import UIKit

let numberOfInstruments = 10
let repeatInterval = 0.01

struct Price {
    let bid: Double;
    let ask: Double;
    let symbol: String;
    let timestamp: NSDate
}

protocol PriceListenerProtocol {
    func receivedNewPrice(price: Price)
}


class PriceGenerator<T: Hashable where T: PriceListenerProtocol> {
    
    //let instruments = ["EURUSD", "AUDUSD", "GOLD", "GBPPLN", "AA", "AAA", "BB", "CC"]
    var instruments: [String] = Array(1...numberOfInstruments)
        .map({num in String(count: num, repeatedValue: Character("A"))})
    var listeners: Set<T> = []
    var timer: NSTimer?
    
    func startGeneratingPrices(){
        timer = NSTimer.schedule(repeatInterval: repeatInterval){
            timer in self.fireNewPrice()
        }
    }
    
    func stopGeneratingPrices(){
        timer?.invalidate()
    }
    
    func generateRandomPrice(timestamp: NSDate) -> Price {
        func generateRandomDoublePrice() -> Double {
            return Double(arc4random()%10000) / 100.0
        }
        return Price(bid: generateRandomDoublePrice(), ask: generateRandomDoublePrice(), symbol: instruments[random() % instruments.count], timestamp: timestamp)
    }
    
    func addPriceListener(listener: T){
        listeners.insert(listener)
    }
    
    func removePriceListener(listener: T){
        listeners.remove(listener)
    }
    
    func sendPriceToListeners(price:Price){
        for l in listeners {
            l.receivedNewPrice(price)
        }
    }
    
    func fireNewPrice(){
        let timestamp = NSDate()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            self.sendPriceToListeners(self.generateRandomPrice(timestamp))
        }
        
    }
    
}


class PriceTableViewCell : UITableViewCell, PriceListenerProtocol {
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var bidLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var deltaTimeLabel: UILabel!
    @IBOutlet weak var tickTimeLabel: UILabel!
    
    static let dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss"
        return dateFormatter
    }()

    
    var symbol: String {
        didSet {
            if symbolLabel != nil {
                symbolLabel.text = self.symbol
            }
        }
    }
    
    
    required override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        self.symbol = "--"
        super.init(style: UITableViewCellStyle.Default, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.symbol = ""
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func receivedNewPrice(price: Price) {
        dispatch_async(dispatch_get_main_queue()) { 
            if price.symbol == self.symbol {
                
                let deltaT = NSDate().timeIntervalSinceDate(price.timestamp)
                self.symbolLabel?.text = price.symbol
                self.deltaTimeLabel?.text = String(format:"%.3f", deltaT)
                self.tickTimeLabel?.text = PriceTableViewCell.dateFormatter.stringFromDate(price.timestamp)
                self.bidLabel?.text = String(price.bid)
                self.askLabel?.text = String(price.ask)
            }
        }
        
    }
    
    
}

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    let priceGenerator = PriceGenerator<PriceTableViewCell>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        for _ in 0...10{
            priceGenerator.fireNewPrice()
        }
        priceGenerator.startGeneratingPrices()
    }
    
    override func viewDidDisappear(animated: Bool) {
        priceGenerator.stopGeneratingPrices()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priceGenerator.instruments.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:PriceTableViewCell = tableView.dequeueReusableCellWithIdentifier("Price Cell") as! PriceTableViewCell
        cell.symbol = priceGenerator.instruments[indexPath.row]
        priceGenerator.addPriceListener(cell)
        return cell
    }
    

}

